FROM quay.io/roboll/helmfile:helm3-v0.135.0
RUN apk add bash curl jq docker python3
RUN curl -sSL https://sdk.cloud.google.com | bash
COPY . .
ENTRYPOINT ["./setup.sh"]
